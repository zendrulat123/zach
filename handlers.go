package main

import (
	"net/http"
)
	type User struct {
	UserId int `json:"id"`
	Username string `json:"username"`
	Friends []*User `json:"friends"`
}

//get handler
func me(w http.ResponseWriter, r *http.Request) {
	allowedHeaders := "Accept, Content-Type, Content-Length, Accept-Encoding, Authorization,X-CSRF-Token"
	w.Header().Set("Access-Control-Allow-Origin", "https://zach-zendrulat352490.codeanyapp.com/me")
	w.Header().Set("Access-Control-Allow-Origin", "https://zach-zendrulat352490.codeanyapp.com/me")

	w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	w.Header().Set("Access-Control-Allow-Headers", allowedHeaders)
	w.Header().Set("Access-Control-Expose-Headers", "Authorization")
	w.WriteHeader(http.StatusOK)

  w.Header().Set("Content-Type", "text/html")
	switch r.Method {
	case http.MethodGet:

		err := tpl.ExecuteTemplate(w, "me.html", nil)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)

		}

		return
	}
		
}


//get handler
func server(w http.ResponseWriter, r *http.Request) {
	allowedHeaders := "Accept, Content-Type, Content-Length, Accept-Encoding, Authorization,X-CSRF-Token"
	w.Header().Set("Access-Control-Allow-Origin", "https://zach-zendrulat352490.codeanyapp.com/me")
	w.Header().Set("Access-Control-Allow-Origin", "https://zach-zendrulat352490.codeanyapp.com/server")
	w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	w.Header().Set("Access-Control-Allow-Headers", allowedHeaders)
	w.Header().Set("Access-Control-Expose-Headers", "Authorization")
	w.WriteHeader(http.StatusOK)
  w.Header().Set("Content-Type", "text/html")
	switch r.Method {
	case http.MethodGet:

		err := tpl.ExecuteTemplate(w, "server.html", nil)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)

		}

		return
	}
		
}

func resources(w http.ResponseWriter, r *http.Request) {
	allowedHeaders := "Accept, Content-Type, Content-Length, Accept-Encoding, Authorization,X-CSRF-Token"
	w.Header().Set("Access-Control-Allow-Origin", "https://zach-zendrulat352490.codeanyapp.com/me")
	w.Header().Set("Access-Control-Allow-Origin", "https://zach-zendrulat352490.codeanyapp.com/server")
	w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	w.Header().Set("Access-Control-Allow-Headers", allowedHeaders)
	w.Header().Set("Access-Control-Expose-Headers", "Authorization")
	w.WriteHeader(http.StatusOK)
  w.Header().Set("Content-Type", "text/html")
	switch r.Method {
	case http.MethodGet:

		err := tpl.ExecuteTemplate(w, "resources.html", nil)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)

		}

		return
	}
		
}