package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	_ "github.com/go-sql-driver/mysql"

	"log"
	"net/http"
)

var err error

type Part struct {
	Id              string `json:"id,omitempty"`
	Serial          string `json:"Serial,omitempty"`
	Name            string `json:"Name,omitempty"`
	Material        string `json:"Material,omitempty"`
	Design          string `json:"Design,omitempty"`
	Type            string `json:"Type,omitempty"`
	ColorFinish     string `json:"ColorFinish,omitempty"`
	ProductFit      string `json:"ProductFit,omitempty"`
	QuantitySold    string `json:"QuantitySold,omitempty"`
	AttachmentStyle string `json:"AttachmentStyle,omitempty"`
	Logo            string `json:"Logo,omitempty"`
	PartNumber      string `json:"PartNumber,omitempty"`
	Notes           string `json:"Notes,omitempty"`
	Image           string `json:"Image,omitempty"`
	Make            string `json:"Make,omitempty"`
	Year            string `json:"Year,omitempty"`
}

var part []Part

//get handler
func search(w http.ResponseWriter, r *http.Request) {

	fmt.Println(w.Header())
	fmt.Println("started")
	fmt.Println("Post is chosen")
	fmt.Println(r.Header.Get("Origin"))
	allowedHeaders := "Accept, Content-Type, Content-Length, Accept-Encoding, Authorization,X-CSRF-Token"
	w.Header().Set("Access-Control-Allow-Origin", "bill2-zendrulat.c9users.io")
	w.Header().Set("Access-Control-Allow-Origin", "https://preview.c9users.io")

	w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	w.Header().Set("Access-Control-Allow-Headers", allowedHeaders)
	w.Header().Set("Access-Control-Expose-Headers", "Authorization")
	w.WriteHeader(http.StatusOK)

	/*get the data from the form
	 ************************************/
	// ParseForm will parse query string values and make r.Form available
	r.ParseForm()

	//r.Form is map of query string parameters
	// its' type is url.Values, which in turn is a map[string][]string
	queryMap := r.Form
	fmt.Fprintf(w, "Post from website! r.PostFrom = %v\n", r.PostForm)
	Serial := r.FormValue("Serial")
	fmt.Fprintf(w, "metas = %s\n", Serial)
	spew.Dump(Serial)

	switch r.Method {
	case "GET":
		err := tpl.ExecuteTemplate(w, "search.html", nil)
		if err != nil {
			log.Fatalln("template didn't execute: ", err)
		}

	case "POST":
		fmt.Println(r.Header.Get("Origin"))
		err := tpl.ExecuteTemplate(w, "search.html", nil)
		if err != nil {
			log.Fatalln("template didn't execute: ", err)
		}

		// Handle POST requests
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			// Error occurred while parsing request body
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		fmt.Println("reqeusted boyd ", r.Body)
		fmt.Println("body ", body)

		w.WriteHeader(http.StatusOK)
		w.Write([]byte(fmt.Sprintf("Query string values: %s\nBody posted: %s", queryMap, body)))
		fmt.Println("querymap is ", queryMap)
		//get the data
		names := strings.Join(queryMap["Name"], ", ")
		spew.Dump(names)
		/*connect to the db
		 ************************************/

		fmt.Println("opening database")
		db, err := sql.Open("mysql", "zendrulat:@/c9")
		if err != nil {
			log.Fatal(err, "didnt hit querymap")
		}
		defer db.Close()
		err = db.Ping()
		if err != nil {
			log.Fatal(err)
		}
		fmt.Println("reached query")
		for rows.Next() {

			err = rows.Scan(&id, &Serial, &Name, &Material, &Design, &Type, &ColorFinish, &ProductFit, &QuantitySold, &AttachmentStyle, &Logo, &PartNumber, &Notes, &Image, &Make, &Year)
			if err != nil {
				fmt.Println("didnt get them all ", Serial)
				log.Fatal(err, "missing query object")
			}

			fmt.Println("this is the link ", Serial)

			part = append(part, Part{Id: id, Serial: Serial, Name: Name, Material: Material, Design: Design, Type: Type, ColorFinish: ColorFinish, ProductFit: ProductFit, QuantitySold: QuantitySold, AttachmentStyle: AttachmentStyle, Logo: Logo, PartNumber: PartNumber, Notes: Notes, Image: Image, Make: Make, Year: Year})
		}

		rows.Close()
		//compare values

		json.NewEncoder(w).Encode(part)
		part = nil
		names = ""
		queryMap = nil
		return

		http.Redirect(w, r, "/links", 301)
		// Other HTTP methods (eg PUT, PATCH, etc) are not handled by the above
		// so inform the client with appropriate status code
		w.WriteHeader(http.StatusMethodNotAllowed)
	default:
		fmt.Fprintf(w, "Sorry, only GET and POST methods are supported.")
	}

}
